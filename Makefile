
LATEX = pdflatex -shell-escape -interaction=nonstopmode -file-line-error

all : cv

cv : resume.tex resume.sty
	$(LATEX) resume.tex


.PHONY = clean

clean :
	rm *.log *.pdf *out



